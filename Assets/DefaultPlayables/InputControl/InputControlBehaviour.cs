﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public class InputControlBehaviour : PlayableBehaviour
{
    // decide if I want to deactivate the input
    [SerializeField] private bool _deactivateInput = false;

    // the script should be deactivated to haven't any chance to control anything
    private MonoBehaviour _script;

    // says if the first frame happened to save the default state of the script
    private bool _firstFrameHappened;

    // save the default state of the script
    private bool _defaultEnabled;

    /// <summary>
    /// determines the logic what should happen when the track is executed
    /// </summary>
    /// <param name="playable"></param>
    /// <param name="info"></param>
    /// <param name="playerData"></param>
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        // get the passed in player data as MonoBehaviour
         _script = playerData as MonoBehaviour;

         // if no MonoBehaviour is to find, return
        if (_script == null)
            return;

        // if the first frame didn't happen yet, save the default state 
        if (!_firstFrameHappened)
        {
            _defaultEnabled = _script.enabled;

            _firstFrameHappened = true;
        }

        // if the input should be deactivated, deactivate it
        if (_deactivateInput)
        {
            _script.enabled = false;
        }
    }

    /// <summary>
    /// when the timeline is over the script get the default state again
    /// </summary>
    /// <param name="playable"></param>
    /// <param name="info"></param>
    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        // set back that the first framed was happened
        _firstFrameHappened = false;

        // if the script is null return
        if (_script == null)
            return;

        // get the default state back
        _script.enabled = _defaultEnabled;

        
    }
}
