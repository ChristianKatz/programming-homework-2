﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class InputControlClip : PlayableAsset, ITimelineClipAsset
{
    // shows in the clip inspector what for options the template provided
    [SerializeField]
    private InputControlBehaviour template = new InputControlBehaviour();

    /// <summary>
    /// allows to create the clip
    /// </summary>
    /// <param name="graph"></param>
    /// <param name="owner"></param>
    /// <returns></returns>
    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        return ScriptPlayable<InputControlBehaviour>.Create(graph, template);
    }

    //determines what we can do when we want to connect two clips together on the same track
    public ClipCaps clipCaps => ClipCaps.None;
}
