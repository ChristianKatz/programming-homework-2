﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

/// <summary>
/// allows to create the track
/// </summary>
[TrackColor(1,0,0)]
[TrackBindingType(typeof(MonoBehaviour))]
[TrackClipType(typeof(InputControlClip))]
public class InputControlTrack : TrackAsset
{

}
