﻿using JetBrains.Annotations;
using UEGP3.Assets.Core;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace UEGP3.Assets.Code.Editor.Core
{
    [CustomPropertyDrawer(typeof(MinMaxFloat))]
    public class VolumePropertyDrawer : PropertyDrawer
    {
        // NOTE: I took this link as a reference, so the part from "position = Editor.... until "position.y...." is just copied, because I didn't know how to create the slider with the shown values: https://gist.github.com/EmpireWorld/f0f518f8b0ca84224e682532cf39214d

        /// <summary>
        /// the GUI for the volume slider
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // set the attribute 
            MinMaxFloat minMaxFloat = attribute as MinMaxFloat;

            // find the properties of the min and max volume that are in the "MinMaxFloat" script
            var minProperty = property.FindPropertyRelative("MinVolume");
            var maxProperty = property.FindPropertyRelative("MaxVolume");

            // begin of the volume property
            EditorGUI.BeginProperty(position, label, property);

            // get the current float values of the properties that are changing in the inspector of the slider
            var min = minProperty.floatValue;
            var max = maxProperty.floatValue;

            // the position of the slider setup
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            // the setup for the left value field
            var left = new Rect(position.x,position.y,position.width / 2 - 11, position.height);
            // the setup for the right value field
            var right = new Rect(position.x + position.width - left.width, position.y, left.width, position.height);
            // the setup for the separation of both fields
            var mid = new Rect(left.xMax, position.y, 22, position.height);
            // the value is changing in the float field, when the slider is moving
            min = Mathf.Clamp(EditorGUI.FloatField(left, min), minMaxFloat.MinVolume, max);
            // the 
            EditorGUI.LabelField(mid, " to ");
            // the value is changing in the float field, when the slider is moving
            max = Mathf.Clamp(EditorGUI.FloatField(right, max), min, minMaxFloat.MaxVolume);
            position.y += 16f;

            // create the MinMax Slider and set the corresponding volume values
            EditorGUI.MinMaxSlider(position, GUIContent.none, ref min, ref max, minMaxFloat.MinVolume, minMaxFloat.MaxVolume);

            // update the Min Volume and Mx Volume when the slider was moved
            minProperty.floatValue = min;
            maxProperty.floatValue = max;

            // end the volume property
            EditorGUI.EndProperty();

        }
    }
}
