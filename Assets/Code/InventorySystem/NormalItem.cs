﻿using UnityEngine;

namespace UEGP3.InventorySystem
{
	[CreateAssetMenu(fileName = "New Item Bag", menuName = "UEGP3/Items/Item")]
	public class NormalItem : Item
	{
		public override void UseItem()
		{
            base.UseItem();
            base.ScriptableAudioEvent.Play(GameObject.Find(GlobalAudioSourceName).GetComponent<AudioSource>());
            Debug.Log($"Using {_itemName} item.");
		}
	}
}