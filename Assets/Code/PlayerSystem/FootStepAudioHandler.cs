﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(AudioSource))]
	public class FootStepAudioHandler : MonoBehaviour
	{
        private AudioSource _audioSource;

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
		}

		// Called as an animation event
		private void DoFootStepSound(ScriptableAudioEvent footScriptableAudioEvent)
        {
            if (footScriptableAudioEvent != null)
            {
                footScriptableAudioEvent.Play(_audioSource);
			}
            
        }
	}
}