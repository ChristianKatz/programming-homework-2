﻿using System;
using UnityEngine;

namespace UEGP3.Assets.Core
{
[Serializable]
    public class MinMaxFloat : PropertyAttribute
    {
        // the minimum value that is changing when the slider is moving
        public float MinVolume;
        // the maximum value that is changing when the slider is moving
        public float MaxVolume;

        /// <summary>
        /// the method where the values of the slider are passed in
        /// </summary>
        /// <param name="minVolume"></param>
        /// <param name="maxVolume"></param>
        public MinMaxFloat(float minVolume, float maxVolume)
        {
            this.MinVolume = minVolume;
            this.MaxVolume = maxVolume;
        }
    }
}
